using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class CustomButton : Button
    {
        public Action OnPointerDownAction = null;
        public Action OnPointerUpAction = null;

        public override void OnPointerDown(PointerEventData eventData)
        {
            OnPointerDownAction?.Invoke();
            base.OnPointerDown(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            OnPointerUpAction?.Invoke();
            base.OnPointerUp(eventData);
        }
    }
}