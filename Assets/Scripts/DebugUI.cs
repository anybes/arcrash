﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DebugUI : MonoBehaviour
{
    #region Inspector Fields

    [SerializeField] private Slider _scaleSlider;
    [SerializeField] private Slider _heightSlider;
    [SerializeField] private Text _scaleValueText;
    [SerializeField] private Text _heightValueText;
    [SerializeField] private Button _restart;
    [SerializeField] private GameObject _arSessionOrigin;

    #endregion

    #region Private Fields

    private GameObject _spawnedObject;
    private Vector3 _spawnedObjectPos;
    private Vector3 _spawnedObjectScale;

    #endregion


    #region Public Fields

    public Action OnRestartClicked;

    #endregion

    #region Public Methods

    public void Initialize()
    {
        gameObject.SetActive(true);

        _restart.onClick.AddListener(OnRestartClicked.Invoke);

        _scaleValueText.text = _scaleSlider.value.ToString();
        _scaleSlider.onValueChanged.AddListener(value =>
        {
            _scaleValueText.text = value.ToString();
            _arSessionOrigin.transform.localScale = Vector3.one * value;
        }); 
    }

    #endregion
}