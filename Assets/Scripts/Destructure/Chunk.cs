using System.Collections.Generic;
using System.Linq;
using Helpers;
using UnityEngine;

namespace Destruction
{
    public class Chunk : MonoBehaviour

    {
        private Dictionary<Joint, Chunk> _jointToChunk = new Dictionary<Joint, Chunk>();
        private Rigidbody _rigidbody;
        private Vector3 _frozenPos;
        private Quaternion _forzenRot;
        private bool _isFrozen;
        public bool HasBrokenLinks { get; private set; }

        private void FixedUpdate()
        {
            if (_isFrozen)
            {
                transform.position = _frozenPos;
                transform.rotation = _forzenRot;
            }
        }

        public void Setup()
        {
            _rigidbody = GetComponent<Rigidbody>();
            Freeze();

            _jointToChunk.Clear();

            var bounds = GetComponent<MeshCollider>().bounds;
            var chunks = Physics.OverlapBox(transform.position, bounds.extents);
            if (chunks.Any(it => it.tag.Equals("Floor")))
            {
                _rigidbody.mass *= 15;
            }

            foreach (var chunk in chunks)
            {
                if (chunk.gameObject == gameObject || !chunk.gameObject.tag.Equals("chunk"))
                    continue;


                var joint = gameObject.AddComponent<FixedJoint>();
                joint.connectedBody = chunk.GetComponent<Rigidbody>();

                joint.breakForce = 900;
                _jointToChunk.Add(joint, joint.connectedBody.GetOrAddComponent<Chunk>());
            }

           
        }
        
        private void OnJointBreak(float breakForce)
        {
            HasBrokenLinks = true;
        }

        public void CleanBrokenLinks()
        {
            var brokenLinks = _jointToChunk.Keys.Where(joint => joint == false).ToList();
           
            foreach (var link in brokenLinks)
            {
                _jointToChunk.Remove(link);
            }

            if (_jointToChunk.Count == 0)
            {
                Destroy(gameObject, 1.5f);
            }

            HasBrokenLinks = false;
        }

        public void Unfreeze()
        {
            _isFrozen = false;
            _rigidbody.constraints = RigidbodyConstraints.None;
            _rigidbody.useGravity = true;
            SetLayer(LayerMask.GetMask("Default"));
        }

        private void Freeze()
        {
            _isFrozen = true;
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            _rigidbody.useGravity = false;
            SetLayer(LayerMask.GetMask("FrozenChunks"));
            _frozenPos = _rigidbody.transform.position;
            _forzenRot = _rigidbody.transform.rotation;
        }

        private void SetLayer(int layer)
        {
            if (gameObject.layer == layer)
                return;

            gameObject.layer = layer;
        }
    }
}