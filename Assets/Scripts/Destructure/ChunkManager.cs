using System.Collections.Generic;
using System.Linq;
using Project.Scripts.Utils;
using UnityEngine;

namespace Destruction
{
    public class ChunkManager : MonoBehaviour
    {
        private List<Chunk> _chunks = new List<Chunk>();

        public void Setup(Rigidbody[] bodies)
        {
            foreach (var body in bodies)
            {
                var chunk = body.GetOrAddComponent<Chunk>();
                chunk.Setup();
                _chunks.Add(chunk);
            }

            foreach (var chunk in _chunks)
            {
                chunk.Unfreeze();
            }
        }

        private void FixedUpdate()
        {
            if (!_chunks.Any(chunk => chunk.HasBrokenLinks)) return;

            foreach (var brokenChunks in _chunks.Where(chunk => chunk.HasBrokenLinks))
            {
                brokenChunks.CleanBrokenLinks();
            }
        }
    }
}