using Destruction;
using UnityEngine;

namespace DefaultNamespace
{
    public class MeshChunks : MonoBehaviour
    {
        private ChunkManager _chunkManager;

        private void Start()
        {
            _chunkManager = gameObject.AddComponent<ChunkManager>();
            _chunkManager.Setup(gameObject.GetComponentsInChildren<Rigidbody>());
        }
    }
}