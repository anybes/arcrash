using UnityEngine;

namespace DefaultNamespace
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private DebugUI _debugUI;
        [SerializeField] private PlayerController _playerController;
        [SerializeField] private PlaceOnPlane _placeOnPlane;

        private GameObject _placedObject;


        private void Awake()
        {
            Application.targetFrameRate = 60;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            _playerController.PlayerState = EPlayerState.Idle;

            _placeOnPlane.OnObjectPlaced = obj =>
            {
                _playerController.PlayerState = EPlayerState.Idle;

                _debugUI.OnRestartClicked = () =>
                {
                    var bullets = FindObjectsOfType<Bullet>();
                    foreach (var bullet in bullets)
                    {
                        Destroy(bullet);
                    }

                    _placeOnPlane.ReplaceObject();
                    _playerController.PlayerState = EPlayerState.InGame;
                };

                _debugUI.Initialize();
                _playerController.PlayerState = EPlayerState.InGame;
            };
        }
    }
}