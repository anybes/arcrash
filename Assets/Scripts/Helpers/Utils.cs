using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Helpers
{
    public class Utils:Singleton<Utils>
    {
        public bool TryGetTouchPosition(out Vector2 touchPosition)
        {
            if (Input.touchCount > 0)
            {
                touchPosition = Input.GetTouch(0).position;
                return true;
            }

            touchPosition = default;
            return false;
        }

        public void StopPlanesVisualize()
        {
            foreach (var plane in GameObject.FindGameObjectsWithTag("ARPlaneVisualizer"))
            {
                var renderer = plane.GetComponent<Renderer>();
                var visualizer = plane.GetComponent<ARPlaneMeshVisualizer>();
                var colider = plane.GetComponent<MeshCollider>();
                var lineRenderer = plane.GetComponent<LineRenderer>();
                colider.enabled = false;
                renderer.enabled = false;
                visualizer.enabled = false;
                lineRenderer.enabled = false;
            }
        }
    }
}