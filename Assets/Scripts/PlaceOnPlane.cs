﻿using System;
using System.Collections.Generic;
using Helpers;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlaceOnPlane : MonoBehaviour
{
    #region Inspector Fields

    [SerializeField] private GameObject _placedPrefab;

    [SerializeField] private ARSessionOrigin _sessionOrigin;

    #endregion

    #region Public Fields

    public Action<GameObject> OnObjectPlaced;

    #endregion


    #region Private Fields

    private ARRaycastManager _raycastManager;
    private bool _isPlaced;
    private GameObject _spawnedObject;

    #endregion

    #region Static Fields

    private static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    #endregion

    #region Unity Methods

    void Awake()
    {
        _raycastManager = GetComponent<ARRaycastManager>();
    }

    private void Update()
    {
        if (_isPlaced)
            return;

        if (!Utils.Instance.TryGetTouchPosition(out var touchPosition))
            return;

        if (!_raycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon)) return;
        var hitPose = s_Hits[0].pose;

        if (_spawnedObject == null)
        {
            _spawnedObject = Instantiate(_placedPrefab, hitPose.position, _placedPrefab.transform.rotation);
        }
        else
        {
            _spawnedObject.transform.position = hitPose.position;
        }


        _isPlaced = true;
        Utils.Instance.StopPlanesVisualize();
        FindObjectOfType<ARPlaneManager>().enabled = false;
        OnObjectPlaced(_spawnedObject);
    }

    #endregion

    #region Public Methods

    public void ReplaceObject()
    {
        var pos = _spawnedObject.transform.position;
        Destroy(_spawnedObject);
        _spawnedObject = Instantiate(_placedPrefab, pos, _placedPrefab.transform.rotation);
    }

    #endregion
}