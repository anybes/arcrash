using System;
using UnityEngine;

public enum EPlayerState
{
    Idle,
    InGame
}

public class PlayerController : MonoBehaviour
{
    #region Inspector Fields

    [SerializeField] private Weapon _weapon;

    #endregion

    #region Private Fields

    private Action _updateAction = () => { };
    private EPlayerState _playerState = EPlayerState.Idle;
    private bool _isFirstShoot = true;

    #endregion

    #region Properties

    public EPlayerState PlayerState
    {
        get => _playerState;
        set
        {
            _playerState = value;
            switch (value)
            {
                case EPlayerState.Idle:
                {
                    _updateAction = null;
                    break;
                }

                case EPlayerState.InGame:
                {
                    _isFirstShoot = true;
                    _updateAction = UpdateAction;
                    break;
                }
            }
        }
    }

    #endregion

    #region Unity Methods

#if UNITY_EDITOR
    private void Awake()
    {
        PlayerState = EPlayerState.InGame;
    }
#endif
    private void Update()
    {
        _updateAction?.Invoke();
    }

    #endregion

    #region Private Methods

    private void UpdateAction()
    {
#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0))
        {
            if (_isFirstShoot)
            {
                _isFirstShoot = false;
                return;
            }

            _weapon.FireBullet();
        }
#else
        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            if (_isFirstShoot)
            {
              
                _isFirstShoot = false;
                return;
            }
            _weapon.FireBullet();
        }
#endif
    }

    #endregion
}