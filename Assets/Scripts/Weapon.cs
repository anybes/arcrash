using UnityEngine;

public class Weapon : MonoBehaviour
{
    #region Inspector Fields

    [SerializeField] private float _radius = 0.1f;
    [SerializeField] private GameObject _bullet;
    [SerializeField] private float _velocity = 500f;
    [SerializeField] private float _mass = 5f;

    #endregion

    #region Public Methods

    public void FireBullet()
    {
        var bullet = Instantiate(_bullet, Camera.main.transform);
        bullet.transform.localScale = Vector3.one * _radius;

        bullet.transform.localRotation = Quaternion.identity;
        bullet.transform.localPosition = Vector3.zero;
        var rb = bullet.GetComponent<Rigidbody>();
        rb.velocity = bullet.transform.forward * _velocity;
        rb.mass = _mass;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
    }

    #endregion
}